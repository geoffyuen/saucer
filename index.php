<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/4.1.1/normalize.min.css">
	<link rel="stylesheet" href="style.css">
	<style>
		.md {
			white-space: pre-wrap;
			font-family: monospace;
		}		
	</style>
</head>
<body>
	<div class="container flex">
		<div id="readme" class="md flex--col9">
<small>From the readme:</small>

<?php include 'readme.md'; ?>
		</div>
		<div class="md flex--col3">
### No Gulp.<br>No Grunt.<br>All Gain.

Git it on [Gitlab](https://gitlab.com/geoffyuen/saucer).

Built by standing on the shoulders of giants and stackoverflow.
		</div>
	</div>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-rc1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.5/marked.min.js"></script>
	<script>
		$('.md').each(function(){
			var $this = $(this);
			$this.html( marked( $this.html() ) );
			$this.removeClass('md');
		});
	</script>
</body>
</html>