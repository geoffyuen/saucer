# Saucer

It's not a full-blown boilerplate but a small task runner for building websites with html or php.

## Features

- Sass for writing great CSS
- Autoprefixer takes care of browser prefixes
- CSSNano for CSS minification
- Javascript uglifier for compressing scripts
- PHP server for creating static or dynamic sites
- Browser-sync for automatic browser refreshing on save

## Quickstart

Copy into current directory:

`git clone --depth 1 https://gitlab.com/geoffyuen/saucer.git` .

Install dependencies:<sup>*</sup>

`npm install`

Edit the `package.json` to your liking (use the php server? file paths?). 

To start developing:

`npm start`

This will open a browser tab with your site. Start coding!

---

<small>* Probably not a good idea but I like installing the dependancies globally so I don't have to download this shit for every project.</small>